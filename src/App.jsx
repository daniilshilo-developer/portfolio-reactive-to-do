import React from 'react';
import './App.scss';
import TodoList from "./components/TodoList/TodoList";
import TodoInput from "./components/TodoInput/TodoInput";

const App = () => {
    const [todoItems, setTodoItems] = React.useState([
        {
            title: 'Задание 1',
            checked: true,
            key: 'task-1'
        },
        {
            title: 'Ещё одно задание',
            checked: false,
            key: 'task-2'
        },
        {
            title: 'Ещё одно очень длинное название lorem ipsum тебе не поможет, тут просто очень длинное название',
            checked: false,
            key: 'task-3'
        }
    ]);

    function setChecked(id, boolean) {
        console.log(id);
        setTodoItems(todoItems.map(item => {
            if (item.key === id) {
                item.checked = boolean;
            }
            return item;
        }));
    }

    function deleteItem(id) {
        setTodoItems(todoItems.filter((item) => item.key !== id));
        console.log(todoItems);
    }

    function createItem(text) {
        if (text) setTodoItems([...todoItems,{title: text, checked: false, key: `task-${Date.now()}`}]);
    }

    // createItem('Динамически добавленное задание!');

    return (
        <div className="container">
            <div className="App">
                <TodoList items={todoItems} setProp={setChecked} deleteItem={deleteItem}/>
                <TodoInput createItem={createItem}/>
            </div>
        </div>
    );
};

export default App;
