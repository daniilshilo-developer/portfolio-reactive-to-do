import React, {useState} from 'react';
import './TodoInput.scss';

const TodoInput = ({createItem}) => {
    const [val, chVal] = useState('');

    return (
        <form action="#" onSubmit={() => {
            createItem(val)
        }}>
            <input className='todo__input' type="text" value={val} onChange={(event) => {
                chVal(event.target.value)
            }}/>
            <button className='todo__submit' type='submit'>Add!</button>
        </form>
    );
};

export default TodoInput;
