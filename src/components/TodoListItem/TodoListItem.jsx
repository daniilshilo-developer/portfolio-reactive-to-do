import React, {useState} from 'react';
import './TodoListItem.scss';

const TodoListItem = ({title, id, checked = false, set, del}) => {
    /**
     * @brief Функция, которая будет изменять состояние input'а
     */
    function changeChecked() {
        // Изменяем значение состояние input'а
        set(id, !checked);
    }

    /**
     * @brief Функция, которая позволяет удалять элементы по id
     */
    function deleteItem() {
        del(id);
    }

    return (
        <li className="todo__item">
            <label>
                <input type="checkbox" id={id} onChange={changeChecked} checked={checked}/>
                {title}
            </label>
            <div className="todo__rm" onClick={deleteItem}/>
        </li>
    );
};

export default TodoListItem;
