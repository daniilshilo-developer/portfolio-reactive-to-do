import React from 'react';
import styles from './TodoList.module.scss';

// Components
import TodoListItem from "../TodoListItem/TodoListItem";

const TodoList = ({items, setProp, deleteItem}) => {
    return (
        <>
            <h1 className={styles.todo__title}>Todo List</h1>
            <ul className={styles.todo__list}>
                {items.map((task, i) => {return <TodoListItem {...task} id={task.key} set={setProp} del={deleteItem} index={i} />})}
            </ul>
        </>
    );
};

export default TodoList;
